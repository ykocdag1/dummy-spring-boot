FROM maven:3.6.3-jdk-13
COPY . /app 
WORKDIR /app
RUN mvn install -DskipTests
CMD ["java","-jar","target/helloworld-0.0.1-SNAPSHOT.jar"]

